# Horizontal RecyclerViewApp
---
Added locales
---
1. Enter a city
2. Press the search button
3. scroll the RecyclerView
---

Application just after start

<img src="./.raw/start.png"  height="480">

After adding two cities

<img src="./.raw/two.png"  height="480">

After adding one more city

<img src="./.raw/three.png"  height="480">


Error loading the city

<img src="./.raw/error.png"  height="480">

---

Korean locale

<img src="./.raw/locale_kr.png"  height="480">

Polish locale

<img src="./.raw/locale_pl.png"  height="480">