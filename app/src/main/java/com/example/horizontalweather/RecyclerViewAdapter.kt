package com.example.horizontalweather

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.w3c.dom.Text

class RecyclerViewAdapter(private var cityList: ArrayList<CityTemp>) : RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>() {

    private lateinit var itemListener: onItemClickListener
    interface onItemClickListener   {

        fun onItemClick(position: Int) {

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return RecyclerViewHolder(itemView, itemListener)
    }

//    fun update(modelList:ArrayList<CityTemp){
//        cityList = modelList
//        Rec!!.notifyDataSetChanged()
//    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val currentItem = cityList[position]
        holder.cityName.text = currentItem.name

        holder.cityTemp.text = currentItem.temp?.toInt().toString() + " ℃" ?: "??  ℃"
        holder.weatherDescription.text = currentItem.description
        holder.countryCode.text = currentItem.countryCode
//        val url = "$basicPath${currentItem.iconName}@2x.png"
        val url = "https://openweathermap.org/img/wn/${currentItem.iconName}@2x.png"
        Log.d("test", "url: $url")
        Picasso.get().load(url).into(holder.weatherPreview)

    }

    override fun getItemCount(): Int {
        return cityList.size
    }

    fun setOnItemClickListener(listener: onItemClickListener) {
        this.itemListener = listener
    }

    class RecyclerViewHolder(itemView: View, listener: onItemClickListener) : RecyclerView.ViewHolder(itemView){
        val cityName: TextView = itemView.findViewById(R.id.city)
        val cityTemp: TextView = itemView.findViewById(R.id.temp)
        val countryCode: TextView = itemView.findViewById(R.id.countryCode)
        val weatherDescription: TextView = itemView.findViewById(R.id.weatherDescription)
        val weatherPreview: ImageView = itemView.findViewById(R.id.weatherPreview)


        init {
            itemView.setOnClickListener {
                listener.onItemClick(adapterPosition)
            }
        }

    }
}