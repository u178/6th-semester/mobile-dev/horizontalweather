package com.example.horizontalweather

import com.google.gson.annotations.SerializedName

data class CityTemp(
    @SerializedName("name")
    var name: String? = null,

    @SerializedName("temp")
    var temp: Float? = null,

    @SerializedName("windSpeed")
    var windSpeed: Float = -1f,

    @SerializedName("description")
    var description: String? = "",

    @SerializedName("countryCode")
    var countryCode: String = "",

    @SerializedName("iconName")
    var iconName: String = ""

    )