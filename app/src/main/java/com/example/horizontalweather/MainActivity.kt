package com.example.horizontalweather

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.JsonParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.InputStream
import java.net.URL
import java.util.*
import kotlin.system.exitProcess

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    private lateinit var newRecyclerView: RecyclerView
    private lateinit var newArrayList: ArrayList<CityTemp>
    lateinit var cityNames: Array<String>
    lateinit var cityTemps: Array<Float>
    private lateinit var gson: Gson
    private lateinit var adapter: RecyclerViewAdapter
    private var API_KEY: String = ""
    private var language = "en"
    lateinit var locale: Locale

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)
        language = intent.getStringExtra("lang").toString()
//        val locale = Locale(item.toString().lowercase())
//        Locale.setDefault(locale)
//        val config = baseContext.resources.configuration
//        config.locale = locale
//        this.resources.updateConfiguration(config, this.resources.displayMetrics)
//        val refresh = Intent(
//            this,
//            MainActivity::class.java
//        )
//        startActivity(refresh)
        val languages = resources.getStringArray(R.array.languages)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setTitle(R.string.app_name)
        toolbar.inflateMenu(R.menu.languages)
        toolbar.setOnMenuItemClickListener {
            item -> val locale = Locale(item.toString().lowercase())
            Locale.setDefault(locale)
            val config = baseContext.resources.configuration
            config.locale = locale
            this.resources.updateConfiguration(config, this.resources.displayMetrics)
            val refresh = Intent(
                this,
                MainActivity::class.java
            )
            refresh.putExtra("lang", item.toString().lowercase())
            refresh.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(refresh); true}


        gson = Gson()
        API_KEY = getString(R.string.API_KEY)

        newRecyclerView = findViewById(R.id.recyclerView)
        newRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        newRecyclerView.setHasFixedSize(true)

        newArrayList = arrayListOf<CityTemp>()


        adapter = RecyclerViewAdapter(newArrayList)
        newRecyclerView.adapter = adapter
        adapter.setOnItemClickListener(object: RecyclerViewAdapter.onItemClickListener{
            override fun onItemClick(position: Int) {
            }
        })

        val et : EditText = findViewById(R.id.editText)
        val b: Button = findViewById(R.id.button)
        b.setOnClickListener {
            if (et.text.toString().trim().isNotEmpty()) {
                val cityName = et.text.toString().trim()

                addCity(cityName)
            }
        }
    }

    private fun addCity(cityName: String) {
        var cityTemp = CityTemp(cityName, -1f, -1f)
        this.newArrayList.add(cityTemp)
        val elemIndex = newArrayList.size - 1
        adapter.notifyDataSetChanged()

        GlobalScope.launch (Dispatchers.IO) {
            val curWeather = getWeather(cityName)
            newArrayList[elemIndex].name = curWeather.city
            newArrayList[elemIndex].temp = curWeather.temp
            newArrayList[elemIndex].description = curWeather.description
            newArrayList[elemIndex].countryCode = curWeather.countryCode
            newArrayList[elemIndex].iconName = curWeather.iconName
            newRecyclerView.post(Runnable { adapter.notifyDataSetChanged() })

        }

    }

    suspend fun getWeather(cityName: String): Weather {
        val weatherURL = "https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=${API_KEY}&units=metric&lang=${language}"
        try {
            val stream = URL(weatherURL).getContent() as InputStream
            val data = Scanner(stream).nextLine()
            Log.d("mytag", data)
            val parser = JsonParser().parse(data).asJsonObject
            if (parser.get("cod").asString == "200") {
                val name = parser.get("name").toString().trim('"')
                val temp = parser.get("main").asJsonObject.get("temp").toString().toFloat()
                val description =
                    parser.get("weather").asJsonArray[0].asJsonObject.get("description").toString()
                        .trim('"')
                val countryCode = parser.get("sys").asJsonObject.get("country").toString().trim('"')
                val iconName = parser.get("weather").asJsonArray[0].asJsonObject.get("icon").toString().trim('"')


                return Weather(temp, description, countryCode, iconName, name)
            }
        } catch (e: Exception) {
            this@MainActivity.runOnUiThread(java.lang.Runnable {
                Toast.makeText(this, "City loading Error!", Toast.LENGTH_LONG).show()
            })

        }

        return Weather(0f, "City loading error", "", "")
    }

    private fun setLocale(langCode: String) {
        Log.d("mytag", langCode)

    }

//    override fun onBackPressed() {
//        val intent = Intent(Intent.ACTION_MAIN)
//        intent.addCategory(Intent.CATEGORY_HOME)
//        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//        startActivity(intent)
//        finish()
//        exitProcess(0)
//    }


}