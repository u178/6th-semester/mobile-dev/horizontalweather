package com.example.horizontalweather

data class Weather(
    val temp: Float,
    val description: String,
    val countryCode: String,
    val iconName: String = "",
    val city: String = ""
) {
}